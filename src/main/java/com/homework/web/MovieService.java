package com.homework.web;
//http://localhost:8080/movieservice/movie/?i=tt0295701
import javax.xml.bind.PropertyException;
import java.io.File;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.json.JSONException;
import org.json.JSONObject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringWriter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.DeserializationFeature;
//http://localhost:8080/rest/movieservice/mo/?t=xxx
@Path("/movieservice")
public class MovieService {

    /**
     * The web server main function, it get the url request.
     * @param i - A valid IMDb ID.
     * @param t - Movie title to search for.
     * @param type - Type of result to return.
     * @param y - Year of release.
     * @param plot - Return short or full plot.
     * @param r - 	The data type to return.
     * @param callback - JSONP callback name.
     * @param v - 	API version (reserved for future use).
     * @return xml with movie info.
     * @throws JAXBException - if one of the other function that been used fails.
     */
    @GET
    @Path("/movie")
    @Produces({"application/xml"})
    public String getMovie(@QueryParam("i") String i,
                           @QueryParam("t") String t,
                           @QueryParam("type") String type,
                           @QueryParam("y") String y,
                           @QueryParam("plot") String plot,
                           @QueryParam("r") String r,
                           @QueryParam("callback") String callback,
                           @QueryParam("v") String v)
            throws JAXBException ,IOException{
        String res = "<result> fail </result>";
        String response = "";
        String title = "";
        String request;


        // if both of them null, the program cant get movie either from OMDB or from fileSystem.
        if(t == null & i == null)
            return res;

        try{
            //search by title
            if(t != null){

                // if title specified - make sure it valid.
                title = fixTiTle(t);

                //build the request String.
                request = buildReq(i, title, type, y, plot, r, callback, v);

                //check if movie info is on filesystem
                MovieFile file = new MovieFile( title );

                if (file.exists()) {
                    res = getFromFileSystem(file.getFile());
                } else {
                    //delete an used file.
                    file.getFile().delete();

                    //send
                    response = sendReq(request);

                    // if send fail
                    if(!response.equals("")) {
                        //extract the title from movie info response
                        JSONObject json = null;
                        try {
                            json = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // OMDB return failure.
                        if (json.length() == 2) {
                            return res;
                        }
                    }
                    //file may be null - get movie as xml
                    res = processOMDBresult(response, file);
                }
            }
            //search by id
            else{
                //build the request String.
                request = buildReq(i, title, type, y, plot, r, callback, v);

                //send
                response = sendReq(request);

                // if send fail
                if(!response.equals("")) {
                        //extract the title from movie info response
                        JSONObject json = null;
                        try {
                            json = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // OMDB return failure.
                        if(json.length() == 2){
                            return res;
                        }
                    // get movie as xml
                    res = processOMDBresult(response,new MovieFile(json.get("Title").toString()));
                }
            }
        } catch (IOException e){
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return res;
    }


    /**
     *  This function is replacing ' ' with '_' in title argument if they exist, it will prevent 400 error code.
     * @param title - the search object name.
     * @return title with '_' instead of spaces.
     */
    //prevent error 400
    private String fixTiTle(String title){ return title.replaceAll(" ", "_").toLowerCase(); }

    /**
     *  This function will create a Movie class instance from the OMDB json response, and return it as XML
     * @param response from OMDB server.
     * @param file file to convert to xml.
     * @return file converted.
     * @throws IOException if json to string is null.
     */
    private String processOMDBresult(String response, MovieFile file) throws JAXBException ,IOException{
        String xml = "";
        try {
            //create a movie class instance and inject the response json to it.
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            JSONObject json = new JSONObject(response);
            Movie movie = mapper.readValue(json.toString(), Movie.class);
            movie.setFromFileOrOmdb("OMDB");
            //convert to xml
            xml = movieAsXml(movie, true, file);
        } catch (JAXBException e) {
            e.getStackTrace();
        } catch (IOException e) {
            e.fillInStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return !( xml.equals("") ) ? xml : null;

    }

    /**
     *  This function will create a Movie class instance from the local file, and will
     *  return it as XML.
     * @param fromFileSystem is the file name to import from file system.
     * @return the file as xml.
     */
    private String getFromFileSystem(File fromFileSystem) throws IllegalArgumentException, JAXBException, NullPointerException{
        Movie movie = null;
        String xml;
        try {
            //create Movie instance from file
            Unmarshaller un = getContext().createUnmarshaller();
            movie = (Movie) un.unmarshal(fromFileSystem);
            movie.setFromFileOrOmdb("FILE_SYSTEM");
        } catch (JAXBException e) {
            e.getCause();
        } catch (IllegalArgumentException e){
            e.getCause();
        } catch (NullPointerException e){
            e.fillInStackTrace();
        } finally {
            //make it xml
            xml = movieAsXml(movie, false ,null);
        }
        return !( xml.equals("") ) ? xml : null;
    }


    /**
     * This function return a context:
     * The JAXBContext class provides the client's entry point to the JAXB API.
     * It provides an abstraction for managing the XML/Java binding information necessary to implement
     * the JAXB binding framework operations: unmarshal, marshal and validate.
     * @return context of JAXBContext to later use/
     * @throws IllegalArgumentException if the parameter contains null ( Movie.class ).
     */
    private JAXBContext getContext() throws IllegalArgumentException {
        try {
            return JAXBContext.newInstance(Movie.class);
        } catch (JAXBException e) {
            e.getCause();
        }
        return null;
    }





    /**
     *  This function will return movie converted to XML, and
     *  save it to local storage if needed.
     * @param movie to convert.
     * @param saveToFile boolean that indicate if the movie is new,
     *                  and need to be save in file system.
     * @param file to save in case the movie is new.
     * @return movie as xml.
     * @throws JAXBException - when there is an error retrieving the given property or value property name.
     *                         in function m.setProperty().
     * @throws NullPointerException - when getContext() return null.
     */
    private String movieAsXml(Movie movie, boolean saveToFile ,MovieFile file) throws JAXBException, NullPointerException{
        try {
            JAXBContext c = null;
            try {
                c = JAXBContext.newInstance(Movie.class);
            } catch (JAXBException e) {
                e.getCause();
            }

            Marshaller m = c.createMarshaller();
            //for pretty-print XML in JAXB
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            final StringWriter w = new StringWriter();

            //convert to XML.
            m.marshal(movie, w);

            //if true will create file.
            if (saveToFile) {
                movie.setFromFileOrOmdb("FILE_SYSTEM");
                m.marshal(movie, new File(file.getPath()));
            }
            return w.toString();

        } catch (PropertyException e){
            e.getCause();
        } catch (NullPointerException e){
            e.getCause();
        } catch (IllegalArgumentException e){
            e.getCause();
        }
        return "";
    }








    /**
     *
     * @param i - A valid IMDb ID.
     * @param t - Movie title to search for.
     * @param type - Type of result to return.
     * @param y - Year of release.
     * @param plot - Return short or full plot.
     * @param r - 	The data type to return.
     * @param callback - JSONP callback name.
     * @param v - 	API version (reserved for future use).
     * @return string to send as request.
     */
    public String buildReq(String i, String t, String type, String y, String plot, String r, String callback, String v) {
        String request = "http://www.omdbapi.com/?apikey=d9a1a503";
        request = (i != null) ? request + "&i=" + i : request;
        request = (t != null) ? request + "&t=" + t : request;
        request = (type != null) ? request + "&type=" + type : request;
        request = (y != null) ? request + "&y=" + y : request;
        request = (plot != null) ? request + "&plot=" + plot : request;
        request = (r != null) ? request + "&r=" + r : request;
        request = (callback != null) ? request + "&callback=" + callback : request;
        request = (v != null) ? request + "&v=" + v : request;
        return request;
    }

    /**
     *
     * @param request is the string that will be sent to OMDB
     * @return xml with movie info.
     * @return the result from OMDB server as String.
     */
    public String sendReq(String request) throws IOException {

        // send request
        URL url = new URL(request);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        StringBuilder content;
        String line;
        content = new StringBuilder();
        while ((line = in.readLine()) != null) {
            content.append(line);
            content.append("\\n");
        }
        con.disconnect();
        return content.toString();
    }


}
