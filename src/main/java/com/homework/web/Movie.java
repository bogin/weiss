package com.homework.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import javax.xml.bind.annotation.XmlRootElement;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Title",
        "Year",
        "Rated",
        "Released",
        "Runtime",
        "Genre",
        "Director",
        "Writer",
        "Actors",
        "Plot",
        "Language",
        "Country",
        "Awards",
        "Poster",
        "Ratings",
        "Metascore",
        "imdbRating",
        "imdbVotes",
        "imdbID",
        "Type",
        "DVD",
        "BoxOffice",
        "Production",
        "Website",
        "Response"
})
@XmlRootElement(name = "Movie")
public class Movie {

    @JsonProperty("Title")
    private String title;
    @JsonProperty("Year")
    private String year;
    @JsonProperty("Rated")
    private String rated;
    @JsonProperty("Released")
    private String released;
    @JsonProperty("Runtime")
    private String runtime;
    @JsonProperty("Genre")
    private String genre;
    @JsonProperty("Director")
    private String director;
    @JsonProperty("Writer")
    private String writer;
    @JsonProperty("Actors")
    private String actors;
    @JsonProperty("Plot")
    private String plot;
    @JsonProperty("Language")
    private String language;
    @JsonProperty("Country")
    private String country;
    @JsonProperty("Awards")
    private String awards;
    @JsonProperty("Poster")
    private String poster;
    @JsonProperty("Ratings")
    private List<Rating> ratings = null;
    @JsonProperty("Metascore")
    private String metascore;
    @JsonProperty("imdbRating")
    private String imdbRating;
    @JsonProperty("imdbVotes")
    private String imdbVotes;
    @JsonProperty("imdbID")
    private String imdbID;
    @JsonProperty("Type")
    private String type;
    @JsonProperty("DVD")
    private String dVD;
    @JsonProperty("BoxOffice")
    private String boxOffice;
    @JsonProperty("Production")
    private String production;
    @JsonProperty("Website")
    private String website;
    @JsonProperty("Response")
    private String response;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getFromFileOrOmdb() {
        return fromFileOrOmdb;
    }

    public void setFromFileOrOmdb(String fromFileOrOmdb) {
        this.fromFileOrOmdb = fromFileOrOmdb;
    }

    private String fromFileOrOmdb;
    public Movie(){

    }

    /**
     *
     * @param genre xml param.
     * @param metascore xml param.
     * @param imdbVotes xml param.
     * @param runtime xml param.
     * @param imdbID xml param.
     * @param type xml param.
     * @param production xml param.
     * @param plot xml param.
     * @param response xml param.
     * @param released xml param.
     * @param imdbRating xml param.
     * @param title xml param.
     * @param actors xml param.
     * @param year xml param.
     * @param writer xml param.
     * @param boxOffice xml param.
     * @param website xml param.
     * @param director xml param.
     * @param dVD xml param.
     * @param country xml param.
     * @param awards xml param.
     * @param poster xml param.
     * @param language xml param.
     * @param rated xml param.
     * @param ratings xml param.
     */
    public Movie(String title, String year, String rated, String released, String runtime, String genre, String director, String writer, String actors, String plot, String language, String country, String awards, String poster, List<Rating> ratings, String metascore, String imdbRating, String imdbVotes, String imdbID, String type, String dVD, String boxOffice, String production, String website, String response) {
        super();
        this.title = title;
        this.year = year;
        this.rated = rated;
        this.released = released;
        this.runtime = runtime;
        this.genre = genre;
        this.director = director;
        this.writer = writer;
        this.actors = actors;
        this.plot = plot;
        this.language = language;
        this.country = country;
        this.awards = awards;
        this.poster = poster;
        this.ratings = ratings;
        this.metascore = metascore;
        this.imdbRating = imdbRating;
        this.imdbVotes = imdbVotes;
        this.imdbID = imdbID;
        this.type = type;
        this.dVD = dVD;
        this.boxOffice = boxOffice;
        this.production = production;
        this.website = website;
        this.response = response;
    }

    @JsonProperty("Title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("Title")
    public void setTitle(String title) {
        this.title = title;
    }

    public Movie withTitle(String title) {
        this.title = title;
        return this;
    }

    @JsonProperty("Year")
    public String getYear() {
        return year;
    }

    @JsonProperty("Year")
    public void setYear(String year) {
        this.year = year;
    }

    public Movie withYear(String year) {
        this.year = year;
        return this;
    }

    @JsonProperty("Rated")
    public String getRated() {
        return rated;
    }

    @JsonProperty("Rated")
    public void setRated(String rated) {
        this.rated = rated;
    }

    public Movie withRated(String rated) {
        this.rated = rated;
        return this;
    }

    @JsonProperty("Released")
    public String getReleased() {
        return released;
    }

    @JsonProperty("Released")
    public void setReleased(String released) {
        this.released = released;
    }

    public Movie withReleased(String released) {
        this.released = released;
        return this;
    }

    @JsonProperty("Runtime")
    public String getRuntime() {
        return runtime;
    }

    @JsonProperty("Runtime")
    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public Movie withRuntime(String runtime) {
        this.runtime = runtime;
        return this;
    }

    @JsonProperty("Genre")
    public String getGenre() {
        return genre;
    }

    @JsonProperty("Genre")
    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Movie withGenre(String genre) {
        this.genre = genre;
        return this;
    }

    @JsonProperty("Director")
    public String getDirector() {
        return director;
    }

    @JsonProperty("Director")
    public void setDirector(String director) {
        this.director = director;
    }

    public Movie withDirector(String director) {
        this.director = director;
        return this;
    }

    @JsonProperty("Writer")
    public String getWriter() {
        return writer;
    }

    @JsonProperty("Writer")
    public void setWriter(String writer) {
        this.writer = writer;
    }

    public Movie withWriter(String writer) {
        this.writer = writer;
        return this;
    }

    @JsonProperty("Actors")
    public String getActors() {
        return actors;
    }

    @JsonProperty("Actors")
    public void setActors(String actors) {
        this.actors = actors;
    }

    public Movie withActors(String actors) {
        this.actors = actors;
        return this;
    }

    @JsonProperty("Plot")
    public String getPlot() {
        return plot;
    }

    @JsonProperty("Plot")
    public void setPlot(String plot) {
        this.plot = plot;
    }

    public Movie withPlot(String plot) {
        this.plot = plot;
        return this;
    }

    @JsonProperty("Language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("Language")
    public void setLanguage(String language) {
        this.language = language;
    }

    public Movie withLanguage(String language) {
        this.language = language;
        return this;
    }

    @JsonProperty("Country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("Country")
    public void setCountry(String country) {
        this.country = country;
    }

    public Movie withCountry(String country) {
        this.country = country;
        return this;
    }

    @JsonProperty("Awards")
    public String getAwards() {
        return awards;
    }

    @JsonProperty("Awards")
    public void setAwards(String awards) {
        this.awards = awards;
    }

    public Movie withAwards(String awards) {
        this.awards = awards;
        return this;
    }

    @JsonProperty("Poster")
    public String getPoster() {
        return poster;
    }

    @JsonProperty("Poster")
    public void setPoster(String poster) {
        this.poster = poster;
    }

    public Movie withPoster(String poster) {
        this.poster = poster;
        return this;
    }

    @JsonProperty("Ratings")
    public List<Rating> getRatings() {
        return ratings;
    }

    @JsonProperty("Ratings")
    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public Movie withRatings(List<Rating> ratings) {
        this.ratings = ratings;
        return this;
    }

    @JsonProperty("Metascore")
    public String getMetascore() {
        return metascore;
    }

    @JsonProperty("Metascore")
    public void setMetascore(String metascore) {
        this.metascore = metascore;
    }

    public Movie withMetascore(String metascore) {
        this.metascore = metascore;
        return this;
    }

    @JsonProperty("imdbRating")
    public String getImdbRating() {
        return imdbRating;
    }

    @JsonProperty("imdbRating")
    public void setImdbRating(String imdbRating) {
        this.imdbRating = imdbRating;
    }

    public Movie withImdbRating(String imdbRating) {
        this.imdbRating = imdbRating;
        return this;
    }

    @JsonProperty("imdbVotes")
    public String getImdbVotes() {
        return imdbVotes;
    }

    @JsonProperty("imdbVotes")
    public void setImdbVotes(String imdbVotes) {
        this.imdbVotes = imdbVotes;
    }

    public Movie withImdbVotes(String imdbVotes) {
        this.imdbVotes = imdbVotes;
        return this;
    }

    @JsonProperty("imdbID")
    public String getImdbID() {
        return imdbID;
    }

    @JsonProperty("imdbID")
    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    public Movie withImdbID(String imdbID) {
        this.imdbID = imdbID;
        return this;
    }

    @JsonProperty("Type")
    public String getType() {
        return type;
    }

    @JsonProperty("Type")
    public void setType(String type) {
        this.type = type;
    }

    public Movie withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("DVD")
    public String getDVD() {
        return dVD;
    }

    @JsonProperty("DVD")
    public void setDVD(String dVD) {
        this.dVD = dVD;
    }

    public Movie withDVD(String dVD) {
        this.dVD = dVD;
        return this;
    }

    @JsonProperty("BoxOffice")
    public String getBoxOffice() {
        return boxOffice;
    }

    @JsonProperty("BoxOffice")
    public void setBoxOffice(String boxOffice) {
        this.boxOffice = boxOffice;
    }

    public Movie withBoxOffice(String boxOffice) {
        this.boxOffice = boxOffice;
        return this;
    }

    @JsonProperty("Production")
    public String getProduction() {
        return production;
    }

    @JsonProperty("Production")
    public void setProduction(String production) {
        this.production = production;
    }

    public Movie withProduction(String production) {
        this.production = production;
        return this;
    }

    @JsonProperty("Website")
    public String getWebsite() {
        return website;
    }

    @JsonProperty("Website")
    public void setWebsite(String website) {
        this.website = website;
    }

    public Movie withWebsite(String website) {
        this.website = website;
        return this;
    }

    @JsonProperty("Response")
    public String getResponse() {
        return response;
    }

    @JsonProperty("Response")
    public void setResponse(String response) {
        this.response = response;
    }

    public Movie withResponse(String response) {
        this.response = response;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Movie withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("title", title).append("year", year).append("rated", rated).append("released", released).append("runtime", runtime).append("genre", genre).append("director", director).append("writer", writer).append("actors", actors).append("plot", plot).append("language", language).append("country", country).append("awards", awards).append("poster", poster).append("ratings", ratings).append("metascore", metascore).append("imdbRating", imdbRating).append("imdbVotes", imdbVotes).append("imdbID", imdbID).append("type", type).append("dVD", dVD).append("boxOffice", boxOffice).append("production", production).append("website", website).append("response", response).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(genre).append(metascore).append(imdbVotes).append(runtime).append(imdbID).append(type).append(production).append(plot).append(response).append(released).append(imdbRating).append(title).append(actors).append(year).append(writer).append(boxOffice).append(website).append(director).append(dVD).append(country).append(additionalProperties).append(awards).append(poster).append(language).append(rated).append(ratings).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Movie) == false) {
            return false;
        }
        Movie rhs = ((Movie) other);
        return new EqualsBuilder().append(genre, rhs.genre).append(metascore, rhs.metascore).append(imdbVotes, rhs.imdbVotes).append(runtime, rhs.runtime).append(imdbID, rhs.imdbID).append(type, rhs.type).append(production, rhs.production).append(plot, rhs.plot).append(response, rhs.response).append(released, rhs.released).append(imdbRating, rhs.imdbRating).append(title, rhs.title).append(actors, rhs.actors).append(year, rhs.year).append(writer, rhs.writer).append(boxOffice, rhs.boxOffice).append(website, rhs.website).append(director, rhs.director).append(dVD, rhs.dVD).append(country, rhs.country).append(additionalProperties, rhs.additionalProperties).append(awards, rhs.awards).append(poster, rhs.poster).append(language, rhs.language).append(rated, rhs.rated).append(ratings, rhs.ratings).isEquals();
    }

}


