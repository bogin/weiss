package com.homework.web;

import java.io.File;

public class MovieFile {

    private String FILE_NAME ;
    private String PATH_PREFIX = "C:/temp/OMDB/";
    private String PATH_SUFFIX = ".xml";

    public File getFile() {
        return file;
    }

    private File file;
    public String getPath() {
        return PATH;
    }


    public boolean exists(){  return (this.file.exists() && !this.file.isDirectory()) ? true : false; }

    private String PATH;
    public MovieFile(String name) {
        this.FILE_NAME = name;
        this.PATH = PATH_PREFIX + name + PATH_SUFFIX;
        this.file = new File(this.PATH);

    }

    public String getFileName() {
        return FILE_NAME;
    }
}
